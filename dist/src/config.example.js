module.exports = {
    apiNode: "wss://eu.nodes.dxpchain.ws",
    // wss://eu.nodes.dxpchain.ws is an alternative
    useES: true,
    // use elastic search
    esNode: "https://wrapper.elasticsearch.dxpchain.ws",
    botPaymentAccounts: [],
};
