const {groupEntries, parseData} = require("dxpchain-report/dist/src/parser");

const {resolveBlockTimes, resolveAssets} = require("dxpchain-report/dist/src/api/nodeApi");

const {getAccountHistoryES, getAccountHistory} =
    require("dxpchain-report/dist/src/api/getAccountHistory")(true);

module.exports = {
    groupEntries,
    parseData,
    getAccountHistoryES,
    getAccountHistory,
    resolveBlockTimes,
    resolveAssets,
};
